/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whist;

import whist.Card.Rank;
import whist.Card.Suit;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author shashank
 */
public class Deck implements Serializable, Iterable<Card> {

    List<Card> fixedCard = new ArrayList(Arrays.asList(new Card[52]));
    List<Card> spadeCard = new ArrayList(Arrays.asList(new Card[0]));
    private static final long serialVersionUID = 49L;
    private int index;

    public Deck() {
        int i = 0;
        for (Card.Suit suit : Card.Suit.values()) {

            for (Card.Rank rank : Card.Rank.values()) {
                fixedCard.set(i++, new Card(rank, suit));
            }
        }
        Collections.shuffle(fixedCard);
        //System.out.println(fixedCard); 
    }

    public int size() {
        return fixedCard.size();
    }

    public final void newDeck() {
        fixedCard.clear();
        for (Card.Suit suit : Card.Suit.values()) {
            for (Card.Rank rank : Card.Rank.values()) {
                fixedCard.add(new Card(rank, suit));
            }
        }
         Collections.shuffle(fixedCard);

    }

    public List<Card> getCollection() {
        return fixedCard;
    }

    public class deckIterator<Card> implements Iterator<Card> {

        public deckIterator() {
            index = 0;
        }

        @Override
        public boolean hasNext() {
            return index < fixedCard.size();
        }

        @Override
        public Card next() {
            return (Card) fixedCard.get(index++);
        }

        @Override
        public void remove() {
            int i = 0;
            fixedCard.set(i++, null);
        }

    }

    @Override
    public Iterator<Card> iterator() {
        return new deckIterator();
    }

    public List<Card> getFixedCard() {
        return this.fixedCard;
    }

    public Card deal() {
        Iterator iterator = new Deck.deckIterator();
        Card firstCard = new Card(null, null);
        if (iterator.hasNext()) {

            firstCard = fixedCard.get(0);
            fixedCard.set(0, null);

        }

        return firstCard;

    }

    public List<Card> getSpadeIterator() {
        for (int i = 0; i < fixedCard.size(); i++) {
            if (fixedCard.get(i).toString().contains("SPADES")) {
                spadeCard.add(fixedCard.get(i));

            }
        }
        return spadeCard;
    }

    public class SpadeIterator<Card> implements Iterator<Card> {

        public SpadeIterator() {
            getSpadeIterator();
            index = 0;

        }

        @Override
        public boolean hasNext() {

            return index < spadeCard.size();
        }

        @Override
        public Card next() {
            return (Card) spadeCard.get(index++);

        }

    }

    public Iterator<Card> Spadeiterator() {
        return new SpadeIterator();
    }
    
    
    
     
    public void SerializationMethod(){
        Deck newObject = new Deck();
         String filename = "Deck.ser";
        try (FileOutputStream fos = new FileOutputStream(filename))
        {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(newObject);
            oos.close();
        }
        catch (Exception e) {
            e.printStackTrace();}
        
        try (FileInputStream fis = new FileInputStream(filename))
        {
            ObjectInputStream ois = new ObjectInputStream(fis);
            Deck deck2 = (Deck) ois.readObject();
            ois.close();
            deck2.toString();
        }
        catch (Exception e) {
        e.printStackTrace();}
    
    
    }

    public static void main(String[] args) {
        Deck n = new Deck();

//        System.out.println(n.deal());
//        for (int i = 0; i < 6; i++) {
//                n.Spadeiterator();
//        }
//        for (Iterator<Card> it = n.Spadeiterator(); it.hasNext();) {
//            System.out.println(it.next());
//
//        }
        
        n.SerializationMethod();

//        Iterator<Card> it = n.iterator();
//
//        //System.out.println(n.getCollection());
    }
}
