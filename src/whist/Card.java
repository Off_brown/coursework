package whist;
//import java.io.Serializable;

import java.io.Serializable;
import static java.lang.Double.compare;
import java.util.ArrayList;
import java.util.Random;
import java.util.Comparator;

/**
 *
 * @author shashank
 */
public class Card implements Serializable, Comparable<Card> {

    private static final long serialVersionUID = 100L;

    public enum Rank {
        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8),
        NINE(9), TEN(10), JACK(10), QUEEN(10), KING(10), ACE(11);

        final int rankNumber;

        private Rank(int rankNumber) {
            this.rankNumber = rankNumber;
        }

        private static Rank[] vals = values();

        public Rank getNext() {
            return vals[(this.ordinal() + 1) % vals.length];

        }

      

        public int getValue() {
            return rankNumber;
        }
    }

    public enum Suit {
        CLUBS, DIAMONDS, HEARTS, SPADES;

        static Suit randomSuit() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        final Suit random() {
            Suit[] suits = Suit.values();
            Random random = new Random();
            return suits[random.nextInt(suits.length)];
        }

    }

    //private static final Rank[] $VALUES = new Rank[]{};
    private final Rank rank;
    private final Suit suit;

    /* public static Rank[] values()
    {
        return (Rank[])$VALUES.clone();
    }*/
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Suit getSuit() {
        return suit;
    }

    public Rank getRank() {
        return rank;
    }

    @Override
    public int compareTo(Card c) {

        int x = compare(this.rank.ordinal(), c.rank.ordinal());
        if (x == 0) {
            x = compare(this.suit.ordinal(), c.suit.ordinal());
        }
        return x;
    }

    public static Card max(ArrayList<Card> maxCard) {
        Card maxCards = maxCard.get(0);
        for (Card card : maxCard) {
            if (maxCards.compareTo(card) < 0) {
                maxCards = card;
            }
        }
        return maxCards;
    }

    public static class CompareDescending implements Comparator<Card> {

        @Override
        public int compare(Card a, Card b) {
            int x = b.rank.ordinal() - a.rank.ordinal();
            if (x != 0) {
                return x;
            }
            return a.suit.ordinal() - b.suit.ordinal();
        }

    }

    public static ArrayList<Card> chooseGreater(ArrayList<Card> a, Comparator b, Card c) {
        ArrayList<Card> x = new ArrayList<>();
        for (int i = 0; i < a.size(); i++) {
            if (b.compare(c, a.get(i)) < 0) {
                x.add(a.get(i));
            }
        }
        return x;
    }

    //Sorsts card by rank and the  by suits in ascending order 
    public static class CompareRank implements Comparator<Card> {

        @Override
        public int compare(Card a, Card b) {
            int x = a.rank.ordinal() - b.rank.ordinal(); //compare rank first 
            return x; //compare rank if the same 

        }

    }

    public static void selectTest() {

        Comparator<Card> cmp = (Card x, Card y) -> {
            return x.compareTo(y);
        };
        ArrayList<Card> testArray = new ArrayList<>();
        testArray.add(new Card(Rank.ACE, Suit.CLUBS));
        testArray.add(new Card(Rank.TWO, Suit.DIAMONDS));
        testArray.add(new Card(Rank.THREE, Suit.SPADES));
        testArray.add(new Card(Rank.FIVE, Suit.DIAMONDS));
        testArray.add(new Card(Rank.SIX, Suit.SPADES));

        Card x = new Card(Rank.THREE, Suit.DIAMONDS);

        System.out.println(chooseGreater(testArray, cmp, x));

        System.out.println(chooseGreater(testArray, new CompareDescending(), x));

        System.out.println(chooseGreater(testArray, new CompareRank(), x));
    }

    public String toString() {
        return rank + " of " + suit;
    }

    public static void main(String[] args) {
        selectTest();

    }
}
