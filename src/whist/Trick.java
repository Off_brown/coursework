package whist;
import java.util.List;
import whist.Card.Suit;
import whist.Card;


/**
 * Skeleton class for storing information about whist tricks
 * @author ajb
 */
public class Trick{
   public static Suit trumps;
   public List<Card> leadCard;
   
   public Trick(int p){}    //p is the lead player 
   
   public Trick(int p){
   }    //p is the lead player 
   
   public static void setTrumps(Suit s){
       trumps=s;
   }
    
/**
 * 
 * @return the Suit of the lead card.
 */    
    public Suit getLeadSuit(){
        return leadCard.get(0).getSuit();
      
        
    }
/**
 * Records the Card c played by Player p for this trick
 * @param c
 * @param p 
 */
    public void setCard(Card c, Player p){
        throw new UnsupportedOperationException("set card not supported yet."); 
    }
/**
 * Returns the card played by player with id p for this trick
 * @param p
 * @return 
 */    
    public Card getCard(Player p){
        leadCard = (List<Card>) p;
       
        return (Card) leadCard;
        
    }
    
/**
 * Finds the ID of the winner of a completed trick
     * @return 
 */    
    public int findWinner(){
        
        throw new UnsupportedOperationException("get find winner not supported yet."); 
    }
}
