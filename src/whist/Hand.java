package whist;

import whist.Card.CompareRank;
import whist.Card.Rank;
import whist.Card.Suit;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author shashank
 */
public class Hand implements Serializable, Iterable<Card> {

    private final List<Card> cardList;

    //final int[] count = new int[17];
     private  ArrayList<Integer> handValues = new ArrayList<>() ;
    
    
    private static final long serialVersionUID = 300L;
    

    public Hand() {
        this.cardList = new ArrayList<Card>();

    }

    public Hand(Card[] cardList) {
        this();
        this.cardList.addAll(Arrays.asList(cardList));

    }

    public Hand(Hand hand) {
        this();
        this.cardList.addAll(hand.cardList);

    }

    public void add(Card card) {
        this.cardList.add(card);
        
        

    }

    public void add(Collection<Card> card) {
        this.cardList.addAll(card);
        handValues();
    }

    public void add(Hand hand) {
        this.cardList.addAll(cardList);
        handValues();

    }

    public boolean remove(Card card) {
        boolean result = true;
        if (!cardList.remove(card)) {
            result = false;
              handValues();
        }
      
        return result;
        
    }

    public boolean remove(Hand hand) {
        boolean result = true;
        for (Card c : hand.cardList) {
            if (!cardList.remove(c)) {
                result = false;
                handValues();
            }

        }
            
        return result;
    }

    public Card remove(int i) {
        Card c = cardList.remove(i);
        handValues();
        return c;
    }
    
    public void handValues()
    {
        int aceValue= 0;
        //variable holding num of aces
        for (Card cardList1 : cardList) {
            handValues.set(0,handValues.get(0) + cardList1.getRank().rankNumber);
            if (cardList1.getRank()== Card.Rank.ACE)//check if cardList1 rank == ace
            {                
                aceValue++;
                System.out.println(aceValue);                         
                //add to num ace variable
            }
            
        }
        handValues.clear();
        //for (Integer handValue : handValues) {
        for (int i = 0; i < aceValue; i++){
            handValues.add(handValues.get(i)-10);
            //clear the array list hand values
            //loop from 1 to num of aces
            //hand values.add = hand values (i-1) -10
            
        }
    }
            
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (Card c : this.cardList) {
            str.append(c.toString());
            str.append("\n");
        }
        return str.toString();
    }
    
    

    @Override
    public Iterator<Card> iterator() {
        return cardList.iterator();
    }
    
    public void sort ()
    {
        Collections.sort(cardList);
    }
    
    public void sortByRank(){
        Collections.sort(cardList, new CompareRank() );
          handValues();
    }
    
    public int countSuit(Card.Suit suit){
         int increase = 0;
           for (int i = 0; i < cardList.size(); i++) {
               if(cardList.get(i).getSuit()==suit) {
                   increase++;
                     
               } else {
               }
       }
       return increase;
    }
    
    public int countRank(Card.Rank rank)
    {
        int increase = 0;
           for (int i = 0; i < cardList.size(); i++) {
               if(cardList.get(i).getRank()==rank) {
                   increase++;
               } 
               
       }
       return increase;
    
    }
    
    public boolean hasSuit(Suit suit) {
		for (Card card : cardList) {
			if (card.getSuit() == suit) {
				return true;
			}
		}
		
		return false;
	}
    
    
    

   
    public static void main(String[] args) {

        Card[] cards = {new Card(Rank.ACE, Suit.HEARTS), new Card(Rank.FIVE, Suit.HEARTS), new Card(Rank.JACK, Suit.SPADES)};
       
              

        Hand newDeck = new Hand(cards);
        Hand secondDeck = new Hand(newDeck);
        secondDeck.add(new Card(Card.Rank.ACE, Card.Suit.DIAMONDS));
        //System.out.println(secondDeck);

        
        System.out.println(newDeck.countSuit(Card.Suit.HEARTS));
        System.out.println(secondDeck.countRank(Rank.KING));
        System.out.println(secondDeck.hasSuit(Suit.HEARTS));
       // System.out.println(secondDeck.toString());
        //System.out.println(secondDeck);
        //newDeck.remove(card);
        //newDeck.handValues();

        System.out.println(newDeck.handValues);
    }

   

}
